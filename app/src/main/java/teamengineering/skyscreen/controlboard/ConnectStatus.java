package teamengineering.skyscreen.controlboard;

public enum ConnectStatus {
    Idle, BluetoothOff, BluetoothOn, SelectDevice, SelectingDevice, DeviceSelected, Searching, Connecting, Connected, Failed;
}
