package teamengineering.skyscreen.controlboard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class PhotoFrame extends ActionBarActivity implements View.OnTouchListener {
    private static final int minWidth = 150, minHeight = 150;

    RelativeLayout frame;

    ResizableImageView photoView;

    String photoFile;

    ProgressBar loading;

    MenuItem done;

    Context context;

    ImageView tl, tr, bl, br, outline;

    int handleSize, handlePadding;

    boolean touching = false;

    int statusHeight, titleHeight;

    boolean resetCrop = false;

    float startX, startY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getApplicationContext();

        setContentView(R.layout.photo_frame);

        frame = (RelativeLayout)findViewById(R.id.frame);

        loading = (ProgressBar)frame.findViewById(R.id.loading);

        photoView = (ResizableImageView)frame.findViewById(R.id.photo);

        photoView.frame = this;

        handleSize    = (int)getResources().getDimension(R.dimen.handle_size);
        handlePadding = (int)getResources().getDimension(R.dimen.handle_padding);

        tl = (ImageView)frame.findViewById(R.id.tl);
        tr = (ImageView)frame.findViewById(R.id.tr);
        bl = (ImageView)frame.findViewById(R.id.bl);
        br = (ImageView)frame.findViewById(R.id.br);

        outline = (ImageView)frame.findViewById(R.id.ouline);

        tl.setOnTouchListener(this);
        tr.setOnTouchListener(this);
        bl.setOnTouchListener(this);
        br.setOnTouchListener(this);

        outline.setOnTouchListener(this);

        Intent intent = getIntent();

        photoFile = intent.getStringExtra("file");

        Log.d("PHOTO", "Starting downloader");

        new ImageDownloader(this).execute("http://10.5.5.9:8080/videos/DCIM/100GOPRO/" + photoFile);

        statusHeight = getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_photo_frame, menu);

        done = menu.findItem(R.id.done);

        return true;
    }

    public void downloadStarted() {
        Log.d("PHOTO", "Download started");

        if (done != null) done.setVisible(false);

        photoView.setVisibility(View.INVISIBLE);
        loading.setVisibility(View.VISIBLE);
    }

    public void downloadComplete(Bitmap bm) {
        Log.d("PHOTO", "Download complete: " + bm.getByteCount());

        resetCrop = true;

        photoView.setImageBitmap(bm);

        photoView.setVisibility(View.VISIBLE);
        loading.setVisibility(View.INVISIBLE);

        if (done != null) done.setVisible(true);
    }

    public void setImageDimensions(int width, int height) {
        if (!resetCrop) return;

        resetCrop = false;

        tl.setX(100);
        tl.setY(100);

        tr.setX(width - 200);
        tr.setY(100);

        bl.setX(100);
        bl.setY(height - 200);

        br.setX(width - 200);
        br.setY(height - 200);

        outline.setX(100 + handlePadding);
        outline.setY(100 + handlePadding);

        LayoutParams p = outline.getLayoutParams();
        p.width  = (int)(tr.getX() - tl.getX()); // + (handleSize - handlePadding);
        p.height = (int)(bl.getY() - tl.getY());// + (handleSize - handlePadding);

        outline.setLayoutParams(p);
        outline.requestLayout();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.done) {
            Intent intent = new Intent();

            Bundle crop = new Bundle();

            crop.putInt("left", (int)outline.getX());
            crop.putInt("top", (int)outline.getY());
            crop.putInt("width", outline.getWidth());
            crop.putInt("height", outline.getHeight());

            intent.putExtra("dimensions", crop);

            setResult(RESULT_OK, intent);

            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            touching = true;

            startX = event.getRawX();
            startY = event.getRawY() - statusHeight;
        } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
            touching = false;
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            float x = event.getRawX(), y = event.getRawY() - statusHeight;

            LayoutParams p;

            int newWidth, newHeight;

            if (touching) {
                p = outline.getLayoutParams();

                if (v == tl) {
                    newWidth = (int)(tr.getX() - x);
                    newHeight = (int)(bl.getY() - y);

                    if (newWidth >= minWidth) {
                        tl.setX(x);
                        bl.setX(x);
                        outline.setX(x + handlePadding);
                        p.width = newWidth;
                    }

                    if (newHeight >= minHeight) {
                        tl.setY(y);
                        tr.setY(y);
                        outline.setY(y + handlePadding);
                        p.height = newHeight;
                    }
                }
                if (v == tr) {
                    newWidth  = (int)(x - tl.getX());
                    newHeight = (int)(br.getY() - y);

                    if (newWidth >= minWidth) {
                        tr.setX(x);
                        br.setX(x);
                        p.width = newWidth;
                    }

                    if (newWidth >= minHeight) {
                        tr.setY(y);
                        tl.setY(y);
                        outline.setY(y + handlePadding);
                        p.height = newHeight;
                    }
                }
                if (v == bl) {
                    newWidth = (int)(br.getX() - x);
                    newHeight = (int)(y - tl.getY());

                    if (newWidth >= minWidth) {
                        bl.setX(x);
                        tl.setX(x);
                        outline.setX(x + handlePadding);
                        p.width = newWidth;
                    }

                    if (newWidth >= minHeight) {
                        bl.setY(y);
                        br.setY(y);
                        p.height = newHeight;
                    }
                }
                if (v == br) {
                    newWidth = (int)(x - bl.getX());
                    newHeight = (int)(y - tr.getY());

                    if (newWidth >= minWidth) {
                        br.setX(x);
                        tr.setX(x);
                        p.width = newWidth;
                    }

                    if (newWidth >= minHeight) {
                        br.setY(y);
                        bl.setY(y);
                        p.height = newHeight;
                    }
                }
                if (v == outline) {
                    int dX = (int)(x - startX), dY = (int)(y - startY);

                    tl.setX(tl.getX() + dX);
                    tl.setY(tl.getY() + dY);

                    tr.setX(tr.getX() + dX);
                    tr.setY(tr.getY() + dY);

                    bl.setX(bl.getX() + dX);
                    bl.setY(bl.getY() + dY);

                    br.setX(br.getX() + dX);
                    br.setY(br.getY() + dY);

                    outline.setX(outline.getX() + dX);
                    outline.setY(outline.getY() + dY);

                    startX = x;
                    startY = y;
                }

                outline.setLayoutParams(p);
            }
        }

        return true;
    }
}
