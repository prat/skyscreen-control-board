package teamengineering.skyscreen.controlboard;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import static teamengineering.skyscreen.controlboard.ConnectStatus.*;

import static android.support.v4.app.ActivityCompat.startActivityForResult;

public class Communicator extends Thread {
    private final ControlBoard activity;

    private volatile Boolean halt     = false;
    private volatile Boolean transmit = false;
    private volatile Boolean pending  = false;
    private volatile String  message  = null;

    private BluetoothAdapter btAdapter;
    private BluetoothSocket  btSocket;
    private BluetoothDevice  btDevice;

    private Set<BluetoothDevice> pairedDevices;

    private String selectedDevice = null;

    private OutputStream btOut;

    private Boolean connected = false;

    public volatile ConnectStatus status = Idle;
    public String statusMessage = "";

    private BroadcastReceiver btReceiver;

    public Communicator(ControlBoard activity) {
        this.activity = activity;

        Log.d("COMMS", "Constructing");

        btReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                bluetoothActivated();
            }
        };

        activity.registerReceiver(btReceiver, new IntentFilter(BluetoothAdapter.ACTION_REQUEST_ENABLE));

        Log.d("COMMS", "Registered reciever");
    }

    public void startSending() {
        Log.d("COMMS", "start sending");

        synchronized (this.transmit) {
            transmit = true;
        }
    }

    public void stopSending() {
        Log.d("COMMS", "stop sending");

        synchronized (this.transmit) {
            transmit = false;
        }
    }

    public void sendMessage(String message) {
        Log.d("COMMS", "send message");
        Log.d("COMMS", message);

        synchronized(this.pending) {
            this.message = message;
            this.pending = true;
        }
    }

    public void bluetoothActivated() {
        Log.d("COMMS", "bluetooth activated");

        synchronized (this.status) {
            status = BluetoothOn;
        }
    }

    public String[] getDeviceList() {
        Log.d("COMMS", "get device list");

        btAdapter = BluetoothAdapter.getDefaultAdapter();

        if(btAdapter.isEnabled()) pairedDevices = btAdapter.getBondedDevices();

        if (pairedDevices != null) {
            ArrayList<String> deviceNames = new ArrayList<String>();

            for (BluetoothDevice dev : pairedDevices) {
                Log.d("COMMS", "Device: " + dev.getName());

                deviceNames.add(dev.getName());
            }

            return (String[])deviceNames.toArray(new String[deviceNames.size()]);
        } else {
            Log.d("COMMS", "No devices");

            return new String[]{};
        }
    }

    public void selectDevice() {
        status = SelectDevice;
    }

    public void setDevice(String name) {
        Log.d("COMMS", "Set device: " + name);

        Settings settings = new Settings(activity);

        settings.setBluetoothHost(name);

        selectedDevice = name;

        if (transmit) {
            synchronized (this.status) {
                status = DeviceSelected;
            }
        }
    }

    public void halt() {
        Log.d("COMMS", "halt");

        synchronized (this.halt) {
            halt = true;

            if (btReceiver != null) {
                try {
                    activity.unregisterReceiver(btReceiver);
                } catch (RuntimeException e) {
                    // Just log the error, no need to do anything about it
                    Log.d("COMMS", "Unregister failed");
                }
            }
        }
    }

    @Override
    public void run() {
        Log.d("COMMS", "Run loop");

        readHostname();

        Log.d("COMMS", "Saved host name: " + selectedDevice);

        while (!halt) {
            Log.d("COMMS", "looping");
            Log.d("COMMS", "Status: " + status.toString());

            if (transmit) {
                Log.d("COMMS", "transmit");

                if (btOut != null) {
                    Log.d("COMMS", "OutputStream exists");
                    status = Connected;
                }

                if (status == Idle) {
                    Log.d("COMMS", "Idle");
                    connect();
                } else if (status == BluetoothOff) {
                    connect();
                } else if (status == BluetoothOn) {
                    Log.d("COMMS", "BluetoothOn");

                    if (selectedDevice == null) {
                        Log.d("COMMS", "No device selected");

                        status = SelectDevice;
                    } else {
                        connect();
                    }
                } else if (status == SelectDevice) {
                    Log.d("COMMS", "Selecting device");

                    status = SelectingDevice;

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            activity.selectBluetoothDevice();
                        }
                    });
                } else if (status == DeviceSelected) {
                    Log.d("COMMS", "device selected");

                    connect();
                } else if (status == Connected) {
                    Log.d("COMMS", "connected");

                    if (btOut == null) {
                        Log.d("COMMS", "OutputStream doesn't exist");

                        status = Idle;
                    } else {
                        Log.d("COMMS", "Pending: " + pending + ", message: " + (message != null));
                        //synchronized (this.pending) {
                            if (pending && (message != null)) {
                                Log.d("COMMS", "writing");

                                try {
                                    /*
                                    Reconnect and open a fresh socket each time?

                                    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //Standard SerialPortService ID

                                    if (btDevice != null) {
                                        btSocket = btDevice.createRfcommSocketToServiceRecord(uuid);
                                        btSocket.connect();
                                        btOut = btSocket.getOutputStream();
                                    }*/

                                    btOut.write(message.getBytes());
                                    btOut.flush();

                                    /*
                                    btOut.close();
                                    btOut = null;
                                    btSocket.close();
                                    btSocket = null;
                                     */

                                    Log.d("COMMS", "written");
                                } catch (IOException e) {
                                    Log.d("COMMS", "IOException");

                                    status = Idle;
                                } finally {
                                    pending = false;
                                }
                            }
                        //}
                    }
                } else if (status == Failed) {
                    Log.d("COMMS", "Attempting to connect again...");

                    connect();
                }
            }

            try {
                sleep(1000);
            } catch (InterruptedException e) {
                halt = true;

                e.printStackTrace();
            }
        }

        Log.d("COMMS", "Exiting Run Loop");

        if (btOut != null) {
            Log.d("COMMS", "Closing OutputStream");

            try {
                btOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void readHostname() {
        Settings settings = new Settings(activity);

        selectedDevice = settings.getBluetoothHost();
    }

    private void connect() {
        Log.d("COMMS", "Connecting...");

        btAdapter = BluetoothAdapter.getDefaultAdapter();

        if(!btAdapter.isEnabled())
        {
            Log.d("COMMS", "Bluetooth is off");

            if (status != BluetoothOff) {
                status = BluetoothOff;

                Log.d("COMMS", "Starting Bluetooth activity");
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivityForResult(enableBluetooth, 0);
            }

            return;
        } else {
            Log.d("COMMS", "Bluetooth is on");

            status = SelectDevice;
        }

        if (selectedDevice == null) {
            Log.d("COMMS", "No device selected");
            return;
        }

        pairedDevices = btAdapter.getBondedDevices();

        Log.d("COMMS", "Getting paired devices");
        for (BluetoothDevice dev : pairedDevices) {
            if (dev.getName().equals(selectedDevice)) {
                btDevice = dev;

                break;
            }
        }

        if (btDevice == null) {
            Log.d("COMMS", "No match with saved device name and paired devices");

            return;
        }

        status = Connecting;

        try {
            Log.d("COMMS", "Opening OutputStream...");

            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //Standard SerialPortService ID
            btSocket = btDevice.createRfcommSocketToServiceRecord(uuid);
            btSocket.connect();
            btOut = btSocket.getOutputStream();

            Log.d("COMMS", "Connected!");

            status = Connected;
        } catch (IOException e) {
            status = Failed;

            Log.d("COMMS", "Connect Exception: " + e.getMessage());

            statusMessage = "Exception: " + e.getMessage();
        }
    }
}
