package teamengineering.skyscreen.controlboard;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GoPro {
    public static final String path = "http://10.5.5.9:8080/videos/DCIM/100GOPRO/";

    private final ControlBoard activity;

    private final Settings settings;

    private String file;

    private String password = null;

    private String returnCode = null;

    public GoPro(ControlBoard activity) {
        this.activity = activity;

        this.settings = new Settings(activity);
    }

    public boolean turnOn() {
        send("bacpac", "PW", "01");

        return true;
    }

    public boolean photoMode() {
        send("camera", "CM", "01");

        return true;
    }

    public boolean takePhoto() {
        send("camera", "SH", "01");

        return true;
    }

    private String send(final String type, final String command, final String parameter) {
        if (password == null) {
            password = settings.getGoProPassword();
        }

        returnCode = null;

        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                returnCode = null;
            }

            @Override
            protected String doInBackground(Void... params) {
                StringBuilder result = new StringBuilder();

                try {
                    URL url = new URL("http://10.5.5.9/" + type + "/" + command + "?t=" + password + "&p=%" + parameter);

                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    try {
                        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

                        String line;

                        while ((line = in.readLine()) != null) {
                            result.append(line);
                        }
                    } finally {
                        urlConnection.disconnect();
                    }
                } catch (MalformedURLException mfue) {
                    Log.d("GOPRO", "Exception: " + mfue.getMessage());

                    return "fail";
                } catch (IOException ioe) {
                    Log.d("GOPRO", "Exception: " + ioe.getMessage());

                    return "fail";
                }

                return result.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                returnCode = result;
            }
        }.execute();

        while (returnCode == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Log.d("PHOTO", "Exception: " + e.getMessage());
            }
        }

        return returnCode;
    }

    public String getPhotoFile() {
        file = null;

        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                file = null;
            }

            @Override
            protected String doInBackground(Void... params) {
                StringBuilder builder = new StringBuilder();

                String html;

                Pattern pattern = Pattern.compile("(?m)(<a class=\"link\" href=\"GOPR.*>GOPR)(\\d*)(\\.JPG<\\/a>)");

                int latest = 0;

                String sequence = "";

                try {
                    Log.d("GOPRO", "Starting");

                    URL url = new URL("http://10.5.5.9:8080/videos/DCIM/100GOPRO/?order=n");

                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    try {
                        BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

                        String line;

                        while ((line = in.readLine()) != null) {
                            builder.append(line);
                        }

                        html = builder.toString();

                        Matcher matcher = pattern.matcher(html);

                        while (matcher.find()) {
                            try {
                                String number = matcher.group(2);

                                int num = Integer.parseInt(number);

                                if (num > latest) {
                                    latest = num;

                                    sequence = number;
                                }
                            } catch (Exception e) {
                                Log.d("GOPRO", "Parse exception: " + e.getMessage());
                            }
                        }
                    } finally {
                        urlConnection.disconnect();
                    }
                } catch (MalformedURLException mfue) {
                    Log.d("GOPRO", "Exception: " + mfue.getMessage());

                    return "";
                } catch (IOException ioe) {
                    Log.d("GOPRO", "Exception: " + ioe.getMessage());

                    return "";
                }

                return ("GOPR" + sequence + ".JPG");
            }

            @Override
            protected void onPostExecute(String url) {
                file = url;
            }
        }.execute();

        while (file == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Log.d("GOPRO", "Exception: " + e.getMessage());
            }
        }

        return file;
    }
}
