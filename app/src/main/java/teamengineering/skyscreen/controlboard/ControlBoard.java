package teamengineering.skyscreen.controlboard;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class ControlBoard extends ActionBarActivity  implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    Communicator comms;

    GoPro goPro;

    String[] patterns = {"Pattern 1", "Pattern 2", "Pattern 3", "Pattern 4"};

    JSON json;

    FrameLayout controlBoard;

    //MenuItem showBrightness, camera, showTempo;
    MenuItem camera;

    SeekBar crossFade;

    LinearLayout patternLeft, patternRight;

    LinearLayout leftSelect, rightSelect;

    TextView leftLabel, rightLabel;

    Button leftPrevious, leftNext;
    Button rightPrevious, rightNext;

    SeekBar lp0, lp1, lp2, lp3;
    SeekBar rp0, rp1, rp2, rp3;

    SeekBar brightnessSlider, tempoSlider;

    //AlertDialog brightnessDialog, tempoDialog;

    AlertDialog bluetoothDialog;

    MenuItem connectMenu, disconnectMenu, selectMenu, goProMenu;

    ControlBoard activity;

    String photoFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_board);

        activity = this;

        json = new JSON();

        controlBoard = (FrameLayout)findViewById(R.id.controlBoard);

        crossFade = (SeekBar)controlBoard.findViewById(R.id.crossFade);

        patternLeft  = (LinearLayout)controlBoard.findViewById(R.id.patternLeft);
        patternRight = (LinearLayout)controlBoard.findViewById(R.id.patternRight);

        leftSelect = (LinearLayout)patternLeft.findViewById(R.id.pattern);
        rightSelect = (LinearLayout)patternRight.findViewById(R.id.pattern);

        leftLabel = (TextView)leftSelect.findViewById(R.id.patternName);
        leftPrevious = (Button)leftSelect.findViewById(R.id.previousPattern);
        leftNext = (Button)leftSelect.findViewById(R.id.nextPattern);

        rightLabel = (TextView)rightSelect.findViewById(R.id.patternName);
        rightPrevious = (Button)rightSelect.findViewById(R.id.previousPattern);
        rightNext = (Button)rightSelect.findViewById(R.id.nextPattern);

        lp0 = (SeekBar)patternLeft.findViewById(R.id.param0);
        lp1 = (SeekBar)patternLeft.findViewById(R.id.param1);
        lp2 = (SeekBar)patternLeft.findViewById(R.id.param2);
        lp3 = (SeekBar)patternLeft.findViewById(R.id.param3);

        Drawable leftThumb  = getResources().getDrawable(R.drawable.left_thumb);
        Drawable rightThumb = getResources().getDrawable(R.drawable.right_thumb);

        lp0.setThumb(leftThumb);
        lp1.setThumb(leftThumb);
        lp2.setThumb(leftThumb);
        lp3.setThumb(leftThumb);

        rp0 = (SeekBar)patternRight.findViewById(R.id.param0);
        rp1 = (SeekBar)patternRight.findViewById(R.id.param1);
        rp2 = (SeekBar)patternRight.findViewById(R.id.param2);
        rp3 = (SeekBar)patternRight.findViewById(R.id.param3);

        rp0.setThumb(rightThumb);
        rp1.setThumb(rightThumb);
        rp2.setThumb(rightThumb);
        rp3.setThumb(rightThumb);

        leftPrevious.setOnClickListener(this);
        leftNext.setOnClickListener(this);

        rightPrevious.setOnClickListener(this);
        rightNext.setOnClickListener(this);

        crossFade.setOnSeekBarChangeListener(this);

        lp0.setOnSeekBarChangeListener(this);
        lp1.setOnSeekBarChangeListener(this);
        lp2.setOnSeekBarChangeListener(this);
        lp3.setOnSeekBarChangeListener(this);

        rp0.setOnSeekBarChangeListener(this);
        rp1.setOnSeekBarChangeListener(this);
        rp2.setOnSeekBarChangeListener(this);
        rp3.setOnSeekBarChangeListener(this);

        leftLabel.setText(patterns[json.patternA]);
        rightLabel.setText(patterns[json.patternB]);

        ///LayoutInflater inflater = this.getLayoutInflater();

        //LinearLayout brightnessLayout = (LinearLayout)inflater.inflate(R.layout.slider, null);
        brightnessSlider = (SeekBar)controlBoard.findViewById(R.id.brightness);
        brightnessSlider.setProgress(json.brightness);
        brightnessSlider.setOnSeekBarChangeListener(this);

        //LinearLayout tempoLayout = (LinearLayout)inflater.inflate(R.layout.slider, null);
        tempoSlider = (SeekBar)controlBoard.findViewById(R.id.tempo);
        tempoSlider.setProgress(json.tempo);
        tempoSlider.setOnSeekBarChangeListener(this);

        /*AlertDialog.Builder brightnessBuilder = new AlertDialog.Builder(this);
        brightnessBuilder.setTitle("Brightness")
                         .setCancelable(true)
                         .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                             @Override
                             public void onClick(DialogInterface dialog, int which) {
                                 dialog.dismiss();
                             }
                         })
                         .setView(brightnessLayout);

        brightnessDialog = brightnessBuilder.create();

        AlertDialog.Builder tempoBuilder = new AlertDialog.Builder(this);
        tempoBuilder.setTitle("Tempo")
                    .setCancelable(true)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setView(tempoLayout);

        tempoDialog = tempoBuilder.create();*/

        comms = new Communicator(this);

        goPro = new GoPro(this);

        comms.start();

        comms.startSending();
    }

    @Override
    public void onStart() {
        comms.startSending();

        super.onStart();
    }

    @Override
    public void onStop() {
        comms.stopSending();

        comms.halt();

        super.onStop();
    }

    @Override
    public void onPause() {
        comms.stopSending();

        super.onPause();
    }

    @Override
    public void onResume() {
        comms.startSending();

        super.onResume();
    }

    @Override
    public void onRestart() {
        comms.startSending();

        super.onRestart();
    }

    private void sendJSON() {
        comms.sendMessage(json.format());

        json.camera = false;
    }

    @Override
    public void onClick(View v) {
        if (v == leftPrevious) {
            if (json.patternA > 0 ) {
                json.patternA--;

                leftLabel.setText(patterns[json.patternA]);
            }
        } else if (v == leftNext) {
            if (json.patternA < (patterns.length - 1)) {
                json.patternA++;

                leftLabel.setText(patterns[json.patternA]);
            }
        } else if (v == rightPrevious) {
            if (json.patternB > 0 ) {
                json.patternB--;

                rightLabel.setText(patterns[json.patternB]);
            }
        } else if (v == rightNext) {
            if (json.patternB < (patterns.length - 1)) {
                json.patternB++;

                rightLabel.setText(patterns[json.patternB]);
            }
        }

        sendJSON();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (seekBar == brightnessSlider) {
            json.brightness = seekBar.getProgress();
        } else if (seekBar == tempoSlider) {
            json.tempo = seekBar.getProgress();
        } else if (seekBar == lp0) {
            json.a0 = seekBar.getProgress();
        } else if (seekBar == lp1) {
            json.a1 = seekBar.getProgress();
        } else if (seekBar == lp2) {
            json.a2 = seekBar.getProgress();
        } else if (seekBar == lp3) {
            json.a3 = seekBar.getProgress();
        } else if (seekBar == rp0) {
            json.b0 = seekBar.getProgress();
        } else if (seekBar == rp1) {
            json.b1 = seekBar.getProgress();
        } else if (seekBar == rp1) {
            json.b2 = seekBar.getProgress();
        } else if (seekBar == rp2) {
            json.b3 = seekBar.getProgress();
        }

        sendJSON();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if((requestCode == 1) && (resultCode == RESULT_OK)) {
            Bundle dimensions = data.getBundleExtra("dimensions");

            json.camera = true;
            json.file   = photoFile;
            json.left   = dimensions.getInt("left");
            json.top    = dimensions.getInt("top");
            json.width  = dimensions.getInt("width");
            json.height = dimensions.getInt("height");

            sendJSON();
        }
    }

    public void selectBluetoothDevice() {
        final String[] devices = comms.getDeviceList();

        AlertDialog.Builder btBuilder = new AlertDialog.Builder(this);
        btBuilder.setTitle("Select Paired Bluetooth Device")
                .setCancelable(true)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick (DialogInterface dialog,int which) {
                        dialog.dismiss();
                    }
                })
                .setItems(devices, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        comms.setDevice(devices[which]);
                        dialog.dismiss();
                    }
                });

        bluetoothDialog = btBuilder.create();

        bluetoothDialog.show();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {}

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_control_board, menu);

        connectMenu = menu.findItem(R.id.connect_bluetooth);
        disconnectMenu = menu.findItem(R.id.disconnect_bluetooth);
        selectMenu = menu.findItem(R.id.select_device);

        disconnectMenu.setVisible(false);

        goProMenu = menu.findItem(R.id.initialise_gopro);

        camera     = menu.findItem(R.id.camera);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.connect_bluetooth) {
            comms.startSending();

            return true;
        } else if (id == R.id.disconnect_bluetooth) {
            comms.stopSending();

            return true;
        } else if (id == R.id.initialise_gopro) {
            goPro.turnOn();
            goPro.photoMode();
        } else if (id == R.id.select_device) {
            comms.selectDevice();

           return true;
        } else if (id == R.id.camera) {
            goPro.takePhoto();

            photoFile = goPro.getPhotoFile();

            Log.d("PHOTO", photoFile);

            Intent showPhoto = new Intent(this, PhotoFrame.class);

            json.file = photoFile;

            showPhoto.putExtra("file", photoFile);

            Log.d("PHOTO", "Loading photo frame");

            startActivityForResult(showPhoto, 1);

            return true;
        }

        /*
         else if (id == R.id.brightness) {
            brightnessDialog.show();

            return true;
        } else if (id == R.id.tempo) {
            tempoDialog.show();

            return true;

         */

        return super.onOptionsItemSelected(item);
    }
}
