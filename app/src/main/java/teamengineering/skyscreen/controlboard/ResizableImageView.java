package teamengineering.skyscreen.controlboard;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ResizableImageView extends ImageView {
    public PhotoFrame frame;

    public int width = 0, height = 0;

    public ResizableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
        Drawable d = getDrawable();

        if(d!=null){
            // ceil not round - avoid thin vertical gaps along the left/right edges
            int h = MeasureSpec.getSize(heightMeasureSpec);
            int w = (int) Math.ceil((float) h * (float) d.getIntrinsicWidth() / (float) d.getIntrinsicHeight());
            setMeasuredDimension(w, h);

            width  = w;
            height = h;

            frame.setImageDimensions(w, h);
        }else{
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

}