package teamengineering.skyscreen.controlboard;

public class JSON {
    public int brightness = 0;

    public int tempo = 0;

    public int patternA = 0;
    public int patternB = 0;

    public int a0 = 0, a1 = 0, a2 = 0, a3 = 0;

    public int b0 = 0, b1 = 0, b2 = 0, b3 = 0;

    public int crossFade = 0;

    public boolean camera = false;

    public String file = "";

    public int left = 0, top = 0, width = 0, height = 0;

    public String format() {
        StringBuilder builder = new StringBuilder();

        builder.append("{\n");

        builder.append("\"brightness\": " + brightness + ",\n");
        builder.append("\"tempo\": " + tempo + ",\n");
        builder.append("\"patternA\": " + patternA + ",\n");
        builder.append("\"patternB\": " + patternB + ",\n");
        builder.append("\"a0\": " + a0 + ",\n");
        builder.append("\"a1\": " + a1 + ",\n");
        builder.append("\"a2\": " + a2 + ",\n");
        builder.append("\"a3\": " + a3 + ",\n");
        builder.append("\"b0\": " + b0 + ",\n");
        builder.append("\"b1\": " + b1 + ",\n");
        builder.append("\"b2\": " + b2 + ",\n");
        builder.append("\"b3\": " + b3 + ",\n");
        builder.append("\"crossFade\": " + crossFade + ",\n");
        builder.append("\"camera\": " + camera + ",\n");
        builder.append("\"file\": \"" + file + "\",\n");
        builder.append("\"left\": " + left + ",\n");
        builder.append("\"top\": " + top + ",\n");
        builder.append("\"width\": " + width + ",\n");
        builder.append("\"height\": " + height + "\n");

        builder.append("}\n");

        return builder.toString();
    }
}