package teamengineering.skyscreen.controlboard;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Settings extends SQLiteOpenHelper implements DatabaseErrorHandler {
    private static final String DATABASE_NAME = "skyscreen.db";
    private static final int DATABASE_VERSION = 3;

    public Settings(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Settings (BluetoothHost TEXT, GoProPassword TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE Settings;");
        db.execSQL("CREATE TABLE Settings (BluetoothHost TEXT, GoProPassword TEXT);");
    }

    @Override
    public void onCorruption(SQLiteDatabase dbObj) {
        // TODO: recreate the database and show an error message
    }

    public String getBluetoothHost() {
        String hostName = null;

        SQLiteDatabase db = getReadableDatabase();

        Cursor results = db.query("Settings", new String[] {"BluetoothHost"}, null, null, null, null, null);

        if (results.moveToFirst()) {
            hostName = results.getString(0);
        }

        if ((hostName != null) && (hostName.length() == 0)) hostName = null;

        return hostName;
    }

    public void setBluetoothHost(String hostName) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("BluetoothHost", hostName);

        int updated = db.update("Settings", values, null, null);

        if (updated == 0) {
            db.insert("Settings", null, values);
        }
    }

    public String getGoProPassword() {
        String password = null;

        SQLiteDatabase db = getReadableDatabase();

        Cursor results = db.query("Settings", new String[] {"GoProPassword"}, null, null, null, null, null);

        if (results.moveToFirst()) {
            password = results.getString(0);
        }

        if ((password != null) && (password.length() == 0)) password = null;

        if (password == null) {

        }

        return password;
    }

    public void setGoProPassword(String password) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("GoProPassword", password);

        int updated = db.update("Settings", values, null, null);

        if (updated == 0) {
            db.insert("Settings", null, values);
        }
    }
}
