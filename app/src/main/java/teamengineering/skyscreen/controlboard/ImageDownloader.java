package teamengineering.skyscreen.controlboard;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;

public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
    PhotoFrame photoFrame;

    public ImageDownloader(PhotoFrame photoFrame) {
        this.photoFrame = photoFrame;

        Log.d("PHOTO", "ImageDownloader created");
    }

    @Override
    protected Bitmap doInBackground(String... urls) {
        Log.d("PHOTO", "doInBackground");

        String photoURL = urls[0];

        Bitmap bm = null;

        try {
            Log.d("PHOTO", "opening input stream");

            InputStream in = new java.net.URL(photoURL).openStream();

            Log.d("PHOTO", "decoding input stream");

            bm = BitmapFactory.decodeStream(in);

            //Thread.sleep(1000);

            //bm = BitmapFactory.decodeResource(photoFrame.getResources(), R.drawable.ic_launcher);

            Log.d("PHOTO", "done decoding");
        } catch (Exception e) {
            Log.d("PHOTO", "Exception: " + e.getMessage());
            e.printStackTrace();
        }

        return bm;
    }

    @Override
    protected void onPreExecute() {
        Log.d("PHOTO", "onPreExecute");

        photoFrame.downloadStarted();
    }

    @Override
    protected void onPostExecute(Bitmap bm) {
        Log.d("PHOTO", "onPostExecute: " + bm.getByteCount());

        photoFrame.downloadComplete(bm);
    }
}